PHPMyAdmin ECS
==============

Docker container which runs PHP MyAdmin (based on the official docker image
https://hub.docker.com/r/phpmyadmin/phpmyadmin/) which sources secrets from AWS SSM

Variables from SSM
------------------

Set the following envioronment variables to tell the container which SSM keys to load

* `SSM_KEY_PMA_PASSWORD`: SSM Key name to load for the `PMA_PASSWORD` env var

See the [official docker repo for phpmyadmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin/)
for an explanation of what these variables do.

You also need to set the following variable to tell AWS which region you want to read the SSM params from

* `SSM_AWS_REGION`: Which region to read the SSM params from
