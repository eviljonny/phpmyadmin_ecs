#!/bin/sh

PMA_PASSWORD=`aws ssm get-parameter --region $SSM_AWS_REGION --name $SSM_KEY_PMA_PASSWORD --with-decryption --output text --query "Parameter.Value"`

if [ $? -ne 0 ]; then
  echo "Failed to lookup PMA_PASSWORD key in SSM key $SSM_KEY_PMA_PASSWORD"
  exit 3
fi

export PMA_PASSWORD

/run.sh
