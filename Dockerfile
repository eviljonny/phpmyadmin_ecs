FROM phpmyadmin/phpmyadmin:4.8

RUN apk add --update \
    py-pip \
  && pip install awscli \
  && apk del py-pip \
  && rm -rf /var/cache/apk/*

COPY ssm-env-entrypoint.sh /ssm-env-entrypoint.sh

ENTRYPOINT [ "/ssm-env-entrypoint.sh" ]
